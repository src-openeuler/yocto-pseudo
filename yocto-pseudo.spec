Name: pseudo
Version: df1d1321fb093283485c387e3c933d2d264e509c
Release: 1
Summary: Pseudo gives fake root capabilities to a normal user

License: LGPLv2+

BuildRequires:	gcc
BuildRequires:	make

%description
The pseudo utility offers a way to run commands in a virtualized root environment

%files

%changelog

* Tue Jul 5 2022 Wayne Ren <renwei41@huawei.com> - df1d1321fb093283485c387e3c933d2d264e509c-1
- Package init
